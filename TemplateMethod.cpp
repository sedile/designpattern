#include <iostream>

class FileProcess {
public:
    /* Man liest und speichert eine PDF Datei anders als eine TextDatei ... */
    virtual void writeFile() = 0;
    virtual void readFile() = 0;

    void processFile(){
        bool writeToFile = false;

        openFile();

        if( writeToFile = userWantsToWrite()){
            writeFile();
        } else {
            readFile();
        }

        if( writeToFile ){
            saveFile();
        }

        closeFile();
    }

    void openFile(){
        std::cout << "Ãffne eine Datei ..." << "\n";
    }

    void saveFile(){
        std::cout << "Speicher die Datei irgendwo ab" << "\n";
    }

    void closeFile(){
        std::cout << "Schliesse die Datei ..." << "\n";
    }

    bool userWantsToWrite(){
        std::cout << "In eine Datei schreiben (j/n)";
        char e;
        std::cin >> e;
        if ( e == 'j' ){
            return true;
        } else {
            return false;
        }
    }
};

class PDFFileProcess : public FileProcess {
public:
    void writeFile() override {
        std::cout << "Schreibe in die PDF-Datei" << "\n";
    }

    void readFile() override {
        std::cout << "Lese die PDF-Datei ein" << "\n";
    }
};

class MP3FileProcess : public FileProcess {
public:
    void writeFile() override {
        std::cout << "Schreibe in die MP3-Datei" << "\n";
    }

    void readFile() override {
        std::cout << "Lese die MP3-Datei ein" << "\n";
    }
};

class TestClass {
public:
    explicit TestClass(){
        FileProcess *pdf = new PDFFileProcess;
        pdf->processFile();
    }

};
