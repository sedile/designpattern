class Point {
private:
    int _x, _y;

    // Konstruktoren und Destruktor nur private
    explicit Point() : _x(0), _y(0) {}
    explicit Point(const Point &cpy) = delete;
    Point& operator=(const Point &mv) = delete;

public:

    virtual ~Point(){}

    // Die einzige Instanz bekommen
    static Point& getInstance() {
        static Point *instance = new Point();
        return *instance;
    }

    void setCoord(int x, int y){
        _x = x;
        _y = y;
    }

    int* getCoord() const {
        int *coords = new int[2];
        *(coords) = _x;
        *(coords + 1) = _y;
        return coords;
    }

    // Aufruf : Point &p = Point::getInstance();

};
