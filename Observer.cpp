#include <iostream>
#include <vector>
#include <algorithm>

class Car {
private:
    short _position;
    std::vector<class Observer*> _observerlist;
public:
    void setPosition(short newPos){
        _position = newPos;
        notifyAllObserver();
    }

    short getPosition() const {
        return _position;
    }

    void registerObserver(Observer *o){
        _observerlist.push_back(o);
    }

    void unregisterObserver(Observer *o){
        _observerlist.erase(std::remove(_observerlist.begin(), _observerlist.end(), o), _observerlist.end());
    }

    void notifyAllObserver();
};

void Car::notifyAllObserver(){
    for(unsigned int i = 0; i < _observerlist.size(); i++){
        //_observerlist[i]->update();
    }
}

class Observer {
private:
    Car *_car;
protected:
    Car* getCar(){
        return _car;
    }
public:
    explicit Observer(Car *car) {
        _car->registerObserver(this);
    };

    virtual void update() = 0;
};

class Left : public Observer {
public:
    explicit Left(Car *car) : Observer(car){}

    void update(){
        short pos = getCar()->getPosition();
        if( pos > 0){
            std::cout << "1 = Rechts" << "\n";
        }
    }
};

class Middle : public Observer {
public:
    explicit Middle(Car *car) : Observer(car){}

    void update(){
        short pos = getCar()->getPosition();
        if( pos == 0){
            std::cout << "0 = Mitte" << "\n";
        }
    }
};

class Right : public Observer {
public:
    explicit Right(Car *car) : Observer(car){}

    void update(){
        short pos = getCar()->getPosition();
        if( pos < 0){
            std::cout << "-1 = Links" << "\n";
        }
    }
};

class TestObserver {
public:
    explicit TestObserver(){
        Car *car = new Car();

        Left *left = new Left(car);
        Middle *middle = new Middle(car);
        Right *right = new Right(car);

        std::cout << "l = links, c = mitte, r = rechts, b = ende" << "\n";

        char pressedBtn;
        bool flag = false;

        while(!flag){
            std::cin >> pressedBtn;

            switch(pressedBtn){
                case 99 : {
                    car->setPosition(0);
                    break;
                }
                case 108 : {
                    car->setPosition(-1);
                    break;
                }
                case 114 : {
                    car->setPosition(1);
                    break;
                }
                case 98 : {
                    flag = true;
                    break;
                }
                default : {
                    std::cout << "unbekannt";
                    break;
                }
            }
        }
    }

};
