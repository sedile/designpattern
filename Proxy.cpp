#include <iostream>
#include <string>

class Verificator {
public:
    virtual void processing() = 0;
};

class RealObject : public Verificator {
private:
    std::string _name;
    unsigned short _id;
public:
    explicit RealObject(std::string name, unsigned short id){
        _name = name;
        _id = id;
    }

    virtual ~RealObject(){}

    void processing(){
        std::cout << _name << " " << _id << "\n";
    }
};

/** Ein Proxy soll das Original verbergen. Es koennen z.B. Fehlereingaben
 * o.Ã¤. abgefangen werden, bzw. gefiltert werden */
class ProxyObject : public Verificator {
private:
    RealObject *_real = nullptr;
    std::string _name;
    short _id;
public:
    explicit ProxyObject(std::string name, short id){
        _name = name;
        _id = id;
    }

    virtual ~ProxyObject(){
        delete _real;
    }

    void processing(){
        if ( _real == nullptr ){
            if ( _id >= 0 && _id <= 255 ){
                _real = new RealObject(_name, _id);
            } else {
                std::cout << "Fehler : " << _id << " ist ungueltig. Abbruch." << "\n";
                return;
            }
        }
        _real->processing();
    }
};
